function addVacationChange(){
	var parent = document.getElementById('changeVacationDateWrapper');
	
	var inputChangeDate = document.createElement('input');
	inputChangeDate.type='text';
	inputChangeDate.placeholder='Дата изменения';
	inputChangeDate.classList.add('vacationChangeDate');
	parent.append(inputChangeDate);
	parent.append(document.createTextNode(" "));
	
	var inputVacationDuration = document.createElement('input');
	inputVacationDuration.type='text';
	inputVacationDuration.placeholder='Новая длительность';
	inputVacationDuration.classList.add('newVacationDuration');
	parent.append(inputVacationDuration);
	parent.append(document.createTextNode(" "));
	
	var removeButton = document.createElement('input');
	removeButton.type = 'button';
	removeButton.value = '-';
	removeButton.title = 'Удалить';
	removeButton.className = 'button-red';
	removeButton.style.opacity = 0;
	removeButton.onclick = function(){
		removeButton.style.opacity = 0;
		inputChangeDate.style.width = 0;
		inputChangeDate.style.opacity = 0;
		inputVacationDuration.style.width = 0;
		inputVacationDuration.style.opacity = 0;
		setTimeout(function(){
			removeButton.nextElementSibling.outerHTML = '';
			removeButton.outerHTML='';
			inputChangeDate.outerHTML = '';
			inputVacationDuration.outerHTML = '';
		}, 300);
	};
	parent.append(removeButton);
	parent.append(document.createElement('br'));
	void removeButton.offsetWidth;
	removeButton.style.opacity = 1;
	inputChangeDate.focus();
}

function addUnpaidVacation(){
	var parent = document.getElementById('unpaidVacationWrapper');
	
	var inputChangeDate = document.createElement('input');
	inputChangeDate.type='text';
	inputChangeDate.placeholder='Дата отпуска';
	inputChangeDate.classList.add('unpaidVacationDate');
	parent.append(inputChangeDate);
	parent.append(document.createTextNode(" "));
	
	var inputVacationDuration = document.createElement('input');
	inputVacationDuration.type='text';
	inputVacationDuration.placeholder='Кол-во дней';
	inputVacationDuration.classList.add('unpaidVacationDuration');
	parent.append(inputVacationDuration);
	parent.append(document.createTextNode(" "));
	
	var removeButton = document.createElement('input');
	removeButton.type = 'button';
	removeButton.value = '-';
	removeButton.title = 'Удалить';
	removeButton.className = 'button-red';
	removeButton.style.opacity = 0;
	removeButton.onclick = function(){
		removeButton.style.opacity = 0;
		inputChangeDate.style.width = 0;
		inputChangeDate.style.opacity = 0;
		inputVacationDuration.style.width = 0;
		inputVacationDuration.style.opacity = 0;
		setTimeout(function(){
			removeButton.nextElementSibling.outerHTML = '';
			removeButton.outerHTML='';
			inputChangeDate.outerHTML = '';
			inputVacationDuration.outerHTML = '';
		}, 300);
	};
	parent.append(removeButton);
	parent.append(document.createElement('br'));
	void removeButton.offsetWidth;
	removeButton.style.opacity = 1;
	inputChangeDate.focus();
}

function count(){
	document.getElementById('totalDays').innerHTML = '';
	var workStart = document.getElementById('startDate').value;
	var workEnd = document.getElementById('dismissDate').value;
	var vacationDayCount = document.getElementById('vacationDayCount').value;
	
	var personExperience = new PersonExperience(workStart, workEnd, vacationDayCount);
	
	var vacationDurationChanges = document.getElementsByClassName('vacationChangeDate');
	var newVacationDuration = document.getElementsByClassName('newVacationDuration');
	
	if(vacationDurationChanges.length > 0){
		for (var i=0; i<vacationDurationChanges.length;i++){
			// l(vacationDurationChanges[i].value);
			// l(newVacationDuration[i].value);
			if ((vacationDurationChanges[i].value=='')||(newVacationDuration.value=='')) continue;
			personExperience.addVacationDurationChange(vacationDurationChanges[i].value, newVacationDuration[i].value);
		}
	}
	
	var unpaidVacationDate = document.getElementsByClassName('unpaidVacationDate');
	var unpaidVacationDuration = document.getElementsByClassName('unpaidVacationDuration');
	
	if (unpaidVacationDate.length > 0){
		for (var i=0; i<unpaidVacationDate.length;i++){
			if ((unpaidVacationDate[i].value=='')||(unpaidVacationDuration.value=='')) continue;
			personExperience.addUnpaidVacation(unpaidVacationDate[i].value, unpaidVacationDuration[i].value);
		}
	}
	
	
	var totalDays = personExperience.getEarnedVacation();
	
	document.querySelector('#totalDays').innerHTML = totalDays;
}







