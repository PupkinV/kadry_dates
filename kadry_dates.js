
Number.prototype.pad = function(size)
{
	var s = String(this);
	while (s.length < (size || 2)) { s = "0" + s };
	return s;
}
Date.prototype.getPeriodEndDate = function()
{
	var endDate = new Date(this);
	endDate.setFullYear(endDate.getFullYear()+1);
	endDate.setDate(endDate.getDate()-1);
	return endDate;
};
Date.prototype.toString = function()
{
	return this.getDate().pad() + '.' + (this.getMonth()*1+1).pad() +'.' + this.getFullYear();
}

String.prototype.toDate = function()
{
	if (this.length in {10:'', 8:''})
	{
		
	};
	var arr = this.toString().split('.');
	var appendix = '';
	if (arr[2].length === 2 )
	{
		appendix = 20;
	}
	return new Date(appendix + arr[2],arr[1]-1,arr[0]);
};

function addPeriod(startDate, endDate)
{
	var tr = document.createElement('tr');
	var td = document.createElement('td');
	td.innerHTML = startDate;
	tr.appendChild(td);

	td = document.createElement('td');
	td.innerHTML = endDate;
	tr.appendChild(td);

	td = document.createElement('td');
	var fullPeriodDayCount = ((startDate.getPeriodEndDate() - startDate)/86400000)+1;
	var periodDayCount = ((endDate - startDate)/86400000)+1
	td.innerHTML = periodDayCount;// + ' из ' + fullPeriodDayCount;
	tr.appendChild(td);

	td = document.createElement('td');
	var receivedDayCount =  Math.round((vacationDayCount/fullPeriodDayCount) * periodDayCount);
	td.innerHTML = receivedDayCount;

	tr.appendChild(td);
	document.getElementById('periods').appendChild(tr);
}

function vacationChange()
{
	var vacationChangeWrapper = document.getElementById('vacationChangeWrapper');
	var vacationChangeDate = document.getElementById('vacationChangeDate');
	var vacationDayChange = document.getElementById('vacationDayChange');
	vacationChangeWrapper.innerHTML = vacationChangeDate.value + " - " + vacationDayChange.value;
}


function isValidDate(strDate)
{
	var arr = strDate.toString().split('.');
	var d = parseInt(arr[0], 10);
	var m = parseInt(arr[1], 10);
	var y = parseInt(arr[2], 10);
	var validDate = new Date(y,m-1,d);

	if (y > 2050 || y < 1900) return false;

	if (d == validDate.getDate() && 
		m == validDate.getMonth()+1 && 
		y == validDate.getFullYear())
		return true
	else
		return false;
}

function fillTable()
{
	var startDate = (document.getElementById('startDate').value).toDate();
	var dismissDate = (document.getElementById('dismissDate').value).toDate();
	var vacationDayCount = (document.getElementById('vacationDayCount')).value;

	while (1)
	{	
		var curPeriodEndDate = startDate.getPeriodEndDate();
		if (dismissDate > curPeriodEndDate)
		{
			addPeriod(startDate, curPeriodEndDate);
			startDate = curPeriodEndDate;
			startDate.setDate(startDate.getDate()+1);
		}
		else 
		{
			startDate.setDate(startDate.getDate());
			addPeriod(startDate, dismissDate);
			break;
		}
	}
}

function onInputChange()
{
	document.getElementById('periods').innerHTML = document.querySelector('#periods tr').outerHTML;
	
	if (!isValidDate(document.getElementById('startDate').value) ||
		!isValidDate(document.getElementById('dismissDate').value))
		return false;

	fillTable();

	
};
fillTable();

