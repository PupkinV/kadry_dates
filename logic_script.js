var ONE_DAY_MILLISECONDS = 24*60*60*1000;
function l(x){ console.log(x);}

Date.prototype.addDays = function(days=1){
	this.setDate(this.getDate() + days);
	return new Date(this);
}
Date.prototype.addMonths = function(months=1){
	this.setMonth(this.getMonth() + months);
	return new Date(this);
}
Date.prototype.addYears = function(years=1){
	this.setFullYear(this.getFullYear() + years);
	return new Date(this);
}

function PersonExperience  (startWorkingDate, dismissDate, vacationDuration){
	
	toDateType = function(strDate){
		var arr = strDate.split('.');
		var appendix = '';
		if (arr[2].length === 2 )
		{
			appendix = 20;
		}
		return new Date(Date.UTC(appendix + arr[2],arr[1]-1,arr[0]));
	};
	//расчет полных месяцев между датами
	//если месяц не полный, то откругляется в большую сторону если дней > 15
	getMonthDiff = function(startDate, endDate, include_last_day=true){

		l(startDate);
		l(endDate);
		if (startDate > endDate){
			l('Error: startDate greater than endDate');
			return false;	
		}
		if (+startDate == +endDate) return 0;
		if (include_last_day)
			endDate = new Date(endDate).addDays();
		
		var yearsDiff = endDate.getFullYear() - startDate.getFullYear();
		
		var monthsDiff = yearsDiff*12 + endDate.getMonth() - startDate.getMonth();
		var daysDiff = endDate.getDate() - startDate.getDate();
		if (daysDiff < 0){
			monthsDiff--;
			var lastMonth = new Date(startDate).addMonths(monthsDiff);
			
			if( +new Date(lastMonth).addMonths(-monthsDiff) != +startDate){
				l('OOOps');
				l(+startDate);
				l(+new Date(lastMonth).addMonths(-monthsDiff));
				lastMonth.setDate(0);
			}
			daysDiff = (endDate - lastMonth) / ONE_DAY_MILLISECONDS;
		}
		if (daysDiff >= 15) monthsDiff++;
		l('Months: ' +monthsDiff);
		l('Days  : ' +daysDiff);
		return monthsDiff;
	}
	
	this.startWorkingDate = toDateType(startWorkingDate);
	this.dismissDate = toDateType(dismissDate);
	// this.vacationDurationChanges = [{
		// "changeDate":toDateType(startWorkingDate),
		// "newDuration":vacationDuration}];
	this.vacationDurationChanges = [];
	this.addVacationDurationChange(startWorkingDate, vacationDuration);
	// l('------------------- ');
	// l(this.vacationDurationChanges);
	this.unpaidVacations = [];
	this.periods = new Array();
}

PersonExperience.prototype.addVacationDurationChange = function(changeDate, newDuration){
	this.vacationDurationChanges.push({
		"changeDate":toDateType(changeDate),
		"newDuration":parseInt(newDuration) 
		});
	this.vacationDurationChanges.sort(function(x,y){
		if (x.changeDate > y.changeDate) return 1;
		else if (x.changeDate < y.changeDate) return -1;
		else return 0;
	});
	l(this.vacationDurationChanges);
};

PersonExperience.prototype.addUnpaidVacation = function(vacationDate, vacationDuration){
	var vacationDate = toDateType(vacationDate);
	var unpaidVacation = {
		"vacationDate": vacationDate,
		"vacationDuration":parseInt(vacationDuration),
	};
	this.unpaidVacations.push(unpaidVacation);
};

PersonExperience.prototype.getEarnedVacation = function(){
	var periodStartDate = this.startWorkingDate;
	var currentVacationDuration = this.vacationDurationChanges[0].newDuration;
	var earnedVacationDays = 0;
	var isLastPeriod = false;
	//пробегаемся по всем периодам
	while (periodStartDate <= this.dismissDate){
		
		var periodEndDate = new Date(periodStartDate).addDays(-1).addYears();
		if(periodEndDate >= this.dismissDate){
			periodEndDate = new Date(this.dismissDate);
			isLastPeriod = true;
			l('This is last period!');
		}
		//count all unpaid vacation days during this period
		var unpaidDaysCount = 0;

		this.unpaidVacations.forEach(function(el){
			
			
			var unpaidVacationStartDate = new Date(el.vacationDate);
			var unpaidVacationEndDate = new Date(el.vacationDate);
			unpaidVacationEndDate.setDate(unpaidVacationEndDate.getDate() + el.vacationDuration - 1);

			
			//был ли отпуск без сохранения в этом периоде
			if(!( 
				((unpaidVacationStartDate >= periodStartDate) && (unpaidVacationStartDate <= periodEndDate)) || 
				((unpaidVacationEndDate>=periodStartDate) && (unpaidVacationEndDate<=periodEndDate)) 
			  )){
				return;
			}
			
			//если отпуск начался в прошлом периоде, а закончится в текущем
			//берем вторую часть отпуска, которая принадлежит текущему периоду
			if( (unpaidVacationStartDate < periodStartDate) &&
				(new Date(unpaidVacationStartDate.getTime() + (el.vacationDuration-1)*ONE_DAY_MILLISECONDS) >= periodStartDate)){
				unpaidVacationStartDate = new Date(periodStartDate);
				l('Change start date');
			}
			
			//если отпуск начался в этом периоде, а заканчивается в другом
			//берем только часть отпуска, которая принадлежит текущему периоду
			if ((unpaidVacationStartDate <= periodEndDate) && (unpaidVacationEndDate > periodEndDate)){
				unpaidVacationEndDate = new Date(periodEndDate);
				l('Change end date');
			}
			
			//расчет количества дней отпуска
			unpaidDaysCount += (unpaidVacationEndDate - unpaidVacationStartDate)/ONE_DAY_MILLISECONDS+1;
			
			l('Start: '+ unpaidVacationStartDate);
			l('End  : '+ unpaidVacationEndDate);
		});
		
		var periodShift = 0;
		if (unpaidDaysCount > 14){
			periodShift = unpaidDaysCount - 14;
		}
		if (isLastPeriod){
			periodEndDate.addDays(-periodShift);
		}
		
		
		l(this.unpaidVacations);
		
		//расчет отработанных полных месяцев и дней
		//и заработанный за это время отпуск
		var fullMonthCount = 0;
		var monthsCountInPeriod = 0;
		var daysCount = 0;
		var middleDate = new Date(periodStartDate);
		
		this.vacationDurationChanges.forEach(function(el){
			// if (el.changeDate == middleDate) currentVacationDuration = el.newDuration;
			if( (el.changeDate >= middleDate) && (el.changeDate <= periodEndDate) ) {
				
				l('WWWWWWWWWWWChange duraton FOUND');
				fullMonthCount = getMonthDiff(middleDate, new Date(el.changeDate), false);
				monthsCountInPeriod += fullMonthCount;
				if(monthsCountInPeriod > 12){
					fullMonthCount -= monthsCountInPeriod-12;
					// l('minus ' +monthsCountInPeriod-12);
				}
				earnedVacationDays += fullMonthCount * currentVacationDuration/12;
				currentVacationDuration = el.newDuration;
				middleDate = new Date(el.changeDate);
				
			}
		});
		// l(getMonthDiff(periodStartDate, periodEndDate));
		fullMonthCount = getMonthDiff(middleDate, periodEndDate);
		monthsCountInPeriod += fullMonthCount;
		if(monthsCountInPeriod > 12){
			fullMonthCount -= (monthsCountInPeriod-12);
			// l('minus ' + (monthsCountInPeriod-12));
		}
		earnedVacationDays += fullMonthCount * currentVacationDuration/12;
		
		l('Earned Vacation Days: '+earnedVacationDays);
		
		
		this.periods.push({
			"periodStartDate": new Date(periodStartDate),
			"periodEndDate": new Date(periodEndDate).addDays(periodShift),
			// "vacationDuration": vacationDuration
		});
		
		
		l('===========');
		periodStartDate = new Date(periodEndDate).addDays(periodShift+1);
	};
	// getMonthDiff(toDateType('29.02.2008'),toDateType('05.06.2008'));
	return earnedVacationDays.toFixed(3);
	
};

// var start = new Date();

// var personExperience = new PersonExperience("01.01.2008", "01.01.2012", 30);
// personExperience.addVacationDurationChange("01.01.2009", 40);
// personExperience.addVacationDurationChange("01.01.2010", 50);
// personExperience.addVacationDurationChange("01.01.2011", 60);
// personExperience.addVacationDurationChange("15.01.2008", 31);
// personExperience.addVacationDurationChange("15.01.2011", 31);
// personExperience.addVacationDurationChange("15.02.2008", 31);


// personExperience.addUnpaidVacation("25.12.2008", 25);
// personExperience.addUnpaidVacation("02.01.2008", 16);




// l(personExperience.getEarnedVacation());
// personExperience.getEarnedVacation();


// l(new Date() - start);
